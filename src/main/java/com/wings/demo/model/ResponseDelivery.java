package com.wings.demo.model;

import lombok.Data;

@Data
public class ResponseDelivery {

	private OutDelivery out;
}
