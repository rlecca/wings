package com.wings.demo.model;

import lombok.Data;

@Data
public class OutCRM {

	private String rowID;
	private String accountId;
	private String lastUpdate;
	private CustomerFromCRM customer;
	private Address address;
	private Order[] order;
	
}
