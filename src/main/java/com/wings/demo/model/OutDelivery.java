package com.wings.demo.model;

import lombok.Data;

@Data
public class OutDelivery {
	
	private String accountId;
	private Address address;
	private Customer customer;
	private String lastUpdate;
	private String rowID;	
	
}
