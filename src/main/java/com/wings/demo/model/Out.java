package com.wings.demo.model;

import lombok.Data;

@Data
public class Out {
	
	private String rowID;
	private String accountId;
	private String profile;
	private String rank;
	private String pv;	
	
}
