package com.wings.demo.model;

import lombok.Data;

@Data
public class Address {
	private String postalCode;
	private String city;
	private String streetAddress;
	private String streetAddressNum;
	private String province;
	
}
