package com.wings.demo.model;

import lombok.Data;

@Data
public class Order {
	
	private String id;
	private String type;
	private String desc;
	
}
