package com.wings.demo.model;

import lombok.Data;

@Data
public class Customer {
	private String firstName;
	private String lastName;
}
