package com.wings.demo.model;

import lombok.Data;

@Data
public class CustomerFromCRM {

	private String firstName;
	private String lastName;
	private String birthDate;
	private String birthCity;
	private String sex;

}
