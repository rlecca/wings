package com.wings.demo.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.wings.demo.model.ResponseCRM;

@Service
public class GetUserCRMDelegate implements JavaDelegate {

	RestOperations restTemplate;
	
	public GetUserCRMDelegate(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		String accountId = execution.getVariable("account_id").toString();

		ResponseCRM response = restTemplate.getForObject(
				"https://wings-api-poc.dauvea.it/demoApplication/app/webServicesStub?svc=getUserFromCRM&account_id="
						+ accountId,
				ResponseCRM.class);

		if (response != null) {
			execution.setVariable("rowIDFromCRM", response.getOut().getRowID());
			execution.setVariable("lastUpdateFromCRM", response.getOut().getLastUpdate());
			ObjectValue customerFromCRM = Variables.objectValue(response.getOut().getCustomer())
					.serializationDataFormat(Variables.SerializationDataFormats.JSON).create();
			execution.setVariable("customerFromCRM", customerFromCRM);
			ObjectValue orderListFromCRM = Variables.objectValue(response.getOut().getOrder())
					.serializationDataFormat(Variables.SerializationDataFormats.JSON).create();
			execution.setVariable("orderListFromCRM", orderListFromCRM);
			ObjectValue addressFromCRM = Variables.objectValue(response.getOut().getAddress())
					.serializationDataFormat(Variables.SerializationDataFormats.JSON).create();
			execution.setVariable("addressFromCRM", addressFromCRM);
		} else {
			System.out.println("### RESPONSE is null");
		}

	}

}
