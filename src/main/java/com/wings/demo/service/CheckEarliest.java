package com.wings.demo.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
public class CheckEarliest implements JavaDelegate{

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		String lastUpdateFromCRM = execution.getVariable("lastUpdateFromCRM").toString();
		String lastUpdateFromDelivery = execution.getVariable("lastUpdate").toString();
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dateDelivery = df.parse(lastUpdateFromDelivery);
		Date dateCRM = df.parse(lastUpdateFromCRM);
		
		if (dateCRM.compareTo(dateDelivery) > 0)
			execution.setVariable("sistema_aggiornato", "DELIVERY");
		else execution.setVariable("sistema_aggiornato", "CRM");
		
	}

}
