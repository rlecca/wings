package com.wings.demo.service;

import java.util.logging.Logger;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import com.wings.demo.model.ResponseDelivery;

@Service
public class GetUserDeliveryDelegate implements JavaDelegate {
	private RestTemplate re;
	private RestOperations restTemplate;
	private final Logger LOGGER = Logger.getLogger(GetUserDeliveryDelegate.class.getName());

    public GetUserDeliveryDelegate(RestTemplateBuilder builder) {
    	
        this.restTemplate = builder.build();
    }

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        //access process variable
        String accountId = execution.getVariable("account_id").toString();
        
        //call REST service to lookup account
        ResponseDelivery resp = (restTemplate.getForObject("https://wings-api-poc.dauvea.it/demoApplication/app/webServicesStub?svc=getUserFromDelivery&account_id="+accountId, ResponseDelivery.class));
        
        //access object in Java, store a new process variable
        if (resp != null) {
        	execution.setVariable("row_id", resp.getOut().getRowID());
        	execution.setVariable("account_id", resp.getOut().getAccountId());
        	execution.setVariable("lastUpdate", resp.getOut().getLastUpdate());
        	execution.setVariable("customer", resp.getOut().getCustomer());
        	execution.setVariable("address", resp.getOut().getAddress());
        } else { LOGGER.info("### RESPONSE is null");}

        //serialize a java object into JSON and stored it in this way so Camunda knows it is JSON
        ObjectValue adJson = Variables
                .objectValue(resp.getOut())
                .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                .create();
        //add json object value as process variable
        execution.setVariable("ad", adJson);
    }
    
}