package com.wings.demo.service;

import java.util.logging.Logger;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.wings.demo.model.Response;

@Service
public class GetUserCommissioningDelegate implements JavaDelegate {

	private RestOperations restTemplate;
	private final Logger LOGGER = Logger.getLogger(GetUserCommissioningDelegate.class.getName());

    public GetUserCommissioningDelegate(RestTemplateBuilder builder) {
    	
        this.restTemplate = builder.build();
    }

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        //access process variable
        String accountId = execution.getVariable("account_id").toString();
        
        //call REST service to lookup account
        Response resp = (restTemplate.getForObject("https://wings-api-poc.dauvea.it/demoApplication/app/webServicesStub?svc=getUserFromCommissioning&account_id="+accountId, Response.class));
        
        //access object in Java, store a new process variable
        if (resp != null) {
        	execution.setVariable("row_id", resp.getOut().getRowID());
        	execution.setVariable("account_id", resp.getOut().getAccountId());
        	execution.setVariable("profile", resp.getOut().getProfile());
        	execution.setVariable("rank", resp.getOut().getRank());
        	execution.setVariable("pv", resp.getOut().getPv());
        } else { LOGGER.info("### RESPONSE is null");}

        //serialize a java object into JSON and stored it in this way so Camunda knows it is JSON
        ObjectValue adJson = Variables
                .objectValue(resp.getOut())
                .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                .create();
        //add json object value as process variable
        execution.setVariable("ad", adJson);
    }
    
}